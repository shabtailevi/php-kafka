<?php

require_once('./vendor/autoload.php');

parse_str($_SERVER['QUERY_STRING'], $query_string);
$data = json_decode($query_string['data'],true);

$writers_schema_json = <<<_JSON
{
    "name":"member",
    "type":"record",
        "fields":[
            {"name":"member_id", "type":"int"},
            {"name":"member_name", "type":"string"}
        ]
}
_JSON;

$io = new AvroStringIO();
// Create a datum writer object

$writers_schema = AvroSchema::parse($writers_schema_json);
$writer = new AvroIODatumWriter($writers_schema);
$data_writer = new AvroDataIOWriter($io, $writer, $writers_schema);


    foreach ($data as $datum){
        try{
            $data_writer->append($datum);
        }
        catch(Exception $ex){
           echo json_encode(['data'=>$data,'Error'=>'Data Is Not An Example Of Schema']);
           return; 
        }
    }   


$data_writer->close();
$binary_string = $io->string();
$read_io = new AvroStringIO($binary_string);
$data_reader = new AvroDataIOReader($read_io, new AvroIODatumReader());

/* */




$producer = new \RdKafka\Producer();
$producer->setLogLevel(LOG_DEBUG);

if ($producer->addBrokers("kafka:9092") < 1) {
    echo "Failed adding brokers\n";
    exit;
}

$topic = $producer->newTopic("alfred.user");

if (!$producer->getMetadata(false, $topic, 2000)) {
    echo "Failed to get metadata, is broker down?\n";
    exit;
}

$topic->produce(RD_KAFKA_PARTITION_UA, 0, $_SERVER['QUERY_STRING']);

echo "Message published\n";
